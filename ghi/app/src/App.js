import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import AttendeesList from './AttendeesList';
import PresentationForm from './PresentationForm'; // Import PresentationForm
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <Router>
      <Nav />
      
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="/presentations/new" element={<PresentationForm />} /> {/* Add route for PresentationForm */}
        </Routes>

    </Router>
  );
}

export default App;
