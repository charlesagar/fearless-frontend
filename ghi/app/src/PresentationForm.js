import React, { useState, useEffect } from 'react';

function PresentationForm() {
  const [presenterName, setPresenterName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conference, setConference] = useState('');
  const [conferences, setConferences] = useState([]);

  // Fetch conferences data when component mounts
  useEffect(() => {
    const fetchConferences = async () => {
      const response = await fetch('http://localhost:8000/api/conferences/');
      const data = await response.json();
      setConferences(data.conferences);
    };

    fetchConferences();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      presenter_name: presenterName,
      presenter_email: presenterEmail,
      company_name: companyName,
      title: title,
      synopsis: synopsis,
      conference: conference,
    };

    const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(presentationUrl, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);

      // Reset form fields
      setPresenterName('');
      setPresenterEmail('');
      setCompanyName('');
      setTitle('');
      setSynopsis('');
      setConference('');
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="mb-3">
              <input onChange={e => setPresenterName(e.target.value)} value={presenterName} placeholder="Presenter Name" required
                     type="text" name="presenter_name" id="presenter_name"
                     className="form-control" />
              <input onChange={e => setPresenterEmail(e.target.value)} value={presenterEmail} placeholder="Presenter Email" required
                     type="email" name="presenter_email" id="presenter_email"
                     className="form-control" />
              <input onChange={e => setCompanyName(e.target.value)} value={companyName} placeholder="Company Name"
                     type="text" name="company_name" id="company_name"
                     className="form-control" />
              <input onChange={e => setTitle(e.target.value)} value={title} placeholder="Title" required
                     type="text" name="title" id="title"
                     className="form-control" />
              <textarea onChange={e => setSynopsis(e.target.value)} value={synopsis} placeholder="Synopsis" required
                        name="synopsis" id="synopsis"
                        className="form-control" rows="3" />
              <select
                required
                name="conference"
                id="conference"
                className="form-select"
                value={conference}
                onChange={e => setConference(e.target.value)}
              >
                <option value="">Choose a conference</option>
                {conferences.map(conference => (
                  <option key={conference.id} value={conference.id}>
                    {conference.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
