import React, { useState, useEffect } from 'react';

function ConferenceList() {
  const [conferences, setConferences] = useState([]);

  // Fetch conferences data when component mounts
  useEffect(() => {
    const fetchConferences = async () => {
      const response = await fetch('http://localhost:8000/api/conferences/');
      const data = await response.json();
      setConferences(data.conferences);
    };

    fetchConferences();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Starts</th>
          <th>Ends</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {conferences.map(conference => {
          return (
            <tr key={conference.href}>
              <td>{ conference.name }</td>
              <td>{ conference.starts }</td>
              <td>{ conference.ends }</td>
              <td>{ conference.location }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ConferenceList;
