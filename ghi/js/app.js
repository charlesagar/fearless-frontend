function createCard(name, description, pictureUrl, date, date2, location) {
    return `
    <div class="col">
    <div class="card mb-3 shadow">
    <img src="${pictureUrl}" class="card-img-top">
    <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle">${location}</h6>
        <p class="card-text">${description}</p>
        <footer class="card-text"><small class="text-muted">Dates:${date}-${date2}</small></footer>
    </div>
    </div>
    </div>
  `;
}



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';



      try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error(`HTTP error! status: ${response.status}`);
            showAlert('Error fetching conferences. Please try again later.');
          // Figure out what to do when the response is bad
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const location = details.conference.location.name
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const date = new Date(details.conference.starts).toLocaleDateString();
              const date2 = new Date(details.conference.ends).toLocaleDateString();
              const html = createCard(title, description, pictureUrl, date, date2, location);
              const row = document.querySelector('.row');
              row.innerHTML += html;
            } else {
                showAlert('Error fetching conference details. Please try again later.');
          }
          }
        }
      } catch (e) {
        console.error(e);
        showAlert('An unexpected error occurred. Please try again later.');
        // Figure out what to do if an error is raised
      }

    });

    function showAlert(message) {
        const alert = document.createElement('div');
        alert.className = 'alert alert-danger';
        alert.role = 'alert';
        alert.innerHTML = message;
        document.body.appendChild(alert);
    }

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json(); // json returns workable code to the data

//         const conference = data.conferences[0]; //get a conference "1st oject"
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;




//         console.log(data);
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();


//         const description = details.conference.description;
//         const descriptionTag = document.querySelector('.card-text');
//         descriptionTag.innerHTML = description;
//         // console.log(details.conference.location.picture_url);

//         const image = details.conference.location.picture_url;
//         const imageTag = document.querySelector(".card-img-top");
//         imageTag.src = image;
//         // console.log(details.conference.location.picture_url);
//       }
//     }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });
