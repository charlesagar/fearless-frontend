window.addEventListener('DOMContentLoaded', async () => {

    // Fetch the available locations from the API
    const locationsUrl = 'http://localhost:8000/api/locations/';
    const locationsResponse = await fetch(locationsUrl);

    if (locationsResponse.ok) {
      const locationsData = await locationsResponse.json();

      const selectTag = document.getElementById('location');
      for (let location of locationsData.locations) {
        // Create an 'option' element
        const optionElement = document.createElement('option');

        // Set the '.value' property of the option element to the
        // location's id
        optionElement.value = location.id;

        // Set the '.innerHTML' property of the option element to
        // the location's name
        optionElement.innerHTML = location.name;

        // Append the option element as a child of the select tag
        selectTag.appendChild(optionElement);
      }
    }

    // Add the form submission event listener here
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      console.log(json);

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }
    });
  });
